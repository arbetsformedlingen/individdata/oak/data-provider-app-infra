# Data Provider App Infra

This project provides deployment pipeline for the Data Provider App.

It uses the Jobtechdev [aardvark](https://gitlab.com/arbetsformedlingen/devops/aardvark) project for the heavy lifting.

See the [aardvark process documentation](https://gitlab.com/arbetsformedlingen/devops/aardvark/-/blob/main/docs/process.md).
